﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(stochos_web.Startup))]
namespace stochos_web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
